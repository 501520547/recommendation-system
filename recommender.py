from numpy import *
from scipy.io import loadmat
from scipy.optimize import minimize
from matplotlib.pyplot import *


def normalizeRatings(Y, R):
    m, n = shape(Y)
    Ymean = zeros(m)
    Ynorm = zeros(shape(Y))
    for i in range(m):
        idx = where(R[i, :] == 1)
        Ymean[i] = mean(Y[i, idx])
        Ynorm[i, idx] = Y[i, idx] - Ymean[i]

    return Ynorm, Ymean


def loadMovieList():
    movieList = []

    with open('movie_ids.txt') as fid:
        for line in fid:
            movieName = line.split(' ', 1)[1].strip()
            movieList.append(movieName)

    return movieList


def serialize(*args):
    return hstack(a.ravel('F') for a in args)


def cofiCostFunc(params, Y, R, num_users, num_movies, num_features, lambda_):
    X = reshape(params[:num_movies * num_features],
                (num_movies, num_features), order='F')
    Theta = reshape(params[num_movies * num_features:],
                    (num_users, num_features), order='F')

    J = 0
    X_grad = zeros(shape(X))
    Theta_grad = zeros(shape(Theta))

    J_temp = (dot(X, Theta.T) - Y)**2
    J = (sum(J_temp[R == 1]) + lambda_ *
         sum(sum(Theta**2)) + lambda_ * sum(sum(X**2))) / 2

    X_grad = dot(((dot(X, Theta.T) - Y) * R), Theta) + lambda_ * X
    Theta_grad = dot(((dot(X, Theta.T) - Y) * R).T, X) + lambda_ * Theta

    grad = hstack((X_grad.ravel('F'), Theta_grad.ravel('F')))
    return J, grad


def checkCostFunction(lambda_=0):
    # Create small problem
    X_t = random.rand(4, 3)
    Theta_t = random.rand(5, 3)

    # Zap out most entries
    Y = dot(X_t, Theta_t.T)
    Y[random.rand(*shape(Y)) > 0.5] = 0
    R = where(Y == 0, 0, 1)

    # Run Gradient Checking
    X = random.randn(*shape(X_t))
    Theta = random.randn(*shape(Theta_t))
    num_users = size(Y, 1)
    num_movies = size(Y, 0)
    num_features = size(Theta_t, 1)

    numgrad = computeNumericalGradient(
        lambda t: cofiCostFunc(
            t, Y, R, num_users, num_movies, num_features, lambda_),
        serialize(X, Theta))

    cost, grad = cofiCostFunc(serialize(X, Theta), Y, R,
                              num_users, num_movies, num_features, lambda_)

    print (column_stack((numgrad, grad)))

    print ('The above two columns you get should be very similar.')
    print ('(Left-Your Numerical Gradient, Right-Analytical Gradient)\n')

    diff = linalg.norm(numgrad - grad) / linalg.norm(numgrad + grad)
    print ('If your backpropagation implementation is correct, then')
    print ('the relative difference will be small (less than 1e-9).')
    print ('\nRelative Difference: %g' % diff)


def computeNumericalGradient(J, theta):
    numgrad = zeros(shape(theta))
    perturb = zeros(shape(theta))
    e = 1e-4
    for p in ndindex(shape(theta)):
        # Set perturbation vector
        perturb[p] = e
        loss1, _ = J(theta - perturb)
        loss2, _ = J(theta + perturb)
        # Compute Numerical Gradient
        numgrad[p] = (loss2 - loss1) / (2 * e)
        perturb[p] = 0

    return numgrad


ex8_movies = loadmat('ex8_movies.mat')
Y = ex8_movies['Y']
R = ex8_movies['R']

ex8_moviesParams = loadmat('ex8_movieParams.mat')

X = ex8_moviesParams['X']
Theta = ex8_moviesParams['Theta']

num_users, num_movies, num_features = 4, 5, 3
X = X[:num_movies, :num_features]
Theta = Theta[:num_users, :num_features]


Y = Y[:num_movies, :num_users]
R = R[:num_movies, :num_users]

#  Evaluate cost function

J, _ = cofiCostFunc(serialize(X, Theta), Y, R, num_users,
                    num_movies, num_features, 0.)

print ('Cost at loaded parameters: %f ' % J)
print ('this value should be about 22.22')

checkCostFunction()

J, _ = cofiCostFunc(serialize(X, Theta), Y, R, num_users,
                    num_movies, num_features, 1.5)

print ('Cost at loaded parameters (lambda = 1.5): %f' % J)
print ('(this value should be about 31.34)')

checkCostFunction(1.5)
movieList = loadMovieList()

my_ratings = zeros(len(movieList))

my_ratings[1 - 1] = 4
my_ratings[98 - 1] = 2
my_ratings[7 - 1] = 3
my_ratings[12 - 1] = 5
my_ratings[54 - 1] = 4
my_ratings[64 - 1] = 5
my_ratings[66 - 1] = 3
my_ratings[69 - 1] = 5
my_ratings[183 - 1] = 4
my_ratings[226 - 1] = 5
my_ratings[355 - 1] = 5

print ('\n\nNew user ratings:')
for rating, name in zip(my_ratings, movieList):
    if rating > 0:
        print ('Rated %d for %s' % (rating, name))

ex8_movies = loadmat('ex8_movies.mat')
Y = ex8_movies['Y']
R = ex8_movies['R']

Y = column_stack((my_ratings, Y))
R = column_stack((my_ratings != 0, R))
print(Y[:,0])
Ynorm, Ymean = normalizeRatings(Y, R)

num_users = size(Y, 1)
num_movies = size(Y, 0)
num_features = 10


X = random.randn(num_movies, num_features)
Theta = random.randn(num_users, num_features)

initial_parameters = serialize(X, Theta)

lambda_ = 10
extra_args = (Y, R, num_users, num_movies, num_features, lambda_)
import sys


def callback(p): sys.stdout.write('.')

res = minimize(cofiCostFunc, initial_parameters, extra_args, method='CG',
               jac=True, options={'maxiter': 100}, callback=callback)
theta = res.x
cost = res.fun

print ("\nFinal cost: %e" % cost)

X = reshape(theta[:num_movies * num_features],
            (num_movies, num_features), order='F')
Theta = reshape(theta[num_movies * num_features:],
                (num_users, num_features), order='F')

p = dot(X, Theta.T)
nr = sum(R, 1)
my_predictions = p[:, 0]
print(my_predictions)
a = p*R
print(column_stack((a[:,0],Y[:,0])))
movieList = loadMovieList()

ix = argsort(my_predictions)
print ('\nTop recommendations for you:')
for j in ix[:-11:-1]:
    print ('Predicting rating %.1f for movie %s' %
           (my_predictions[j], movieList[j]))
